<?php
namespace Drupal\tcxlc\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a '3CX Live Chat' Block.
 *
 * @Block(
 *   id = "tcxlc_block",
 *   admin_label = @Translation("3CX Live Chat block"),
 *   category = @Translation("3CX "),
 * )
 */
class tcxlc_block extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	  
	  $config = $this->getConfiguration();
  
	$snippet = isset($config['callus_snippet']) ? $config['callus_snippet'] : '';
	
	return [
			  '#type' => 'inline_template',
			  '#template' => '{{ var | raw }}',
			  '#context' => [
				'var' => $snippet,
			  ],
			];
  }
  
    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

	$snippet = isset($config['callus_snippet']) ? $config['callus_snippet'] : '';	
	
	$form['tcxlc_callus_header'] =	[
		  '#type' => 'inline_template',
		  '#template' => $this->generateTemplate(),
		  '#context' => [
			'var' => $snippet,
		  ],
		];
		
	$form['tcxlc_callus'] = [
      '#type' => 'textarea',
	  '#rows' => 30,
	  '#cols' => 250,
	  '#attributes' => array(
        'placeholder' => $this->t('Place your Live chat code snippet here'),
      ),
      '#default_value' => isset($config['callus_snippet']) ? $config['callus_snippet'] : '',
    ];

    return $form;
  }
  
  private function generateTemplate(){
	  
	  $result = "
		  <style>
				.tcxlc_script_field{
					width:700px;
					margin-top:15px;
				}
				.tcxlc_form{
					display: flex;
					flex-direction: column;
				}
				.tcxlc_header{
					height: 80px;
					display: flex;
					flex-direction: row;
				}
				
				.tcxlc_header img{
					height:80px;
				}
				
				.tcxlc_label{
					text-align: center;
				}
			</style>
			<div class='tcxlc_form' >
				<div class='tcxlc_header'>
					<div>
						<img src='". file_create_url(drupal_get_path('module', 'tcxlc').'/images/logo.png')."' /> 
					</div>
					<div>
						<ol>
							<li>Configure your <a href='https://www.3cx.com/docs/manual/live-chat/#h.vyesuv769zft' target='_blank'>PBX</a>.</li>
							<li>Generate code snippet <a href='https://www.3cx.com/live-chat/code-generator/' target='_blank'>here</a>.</li>
							<li>Paste the code and start using!</li>
						</ol>
					</div>
				</div>
			</div>
	  ";
	  
	  return $result;
	  
  }
  
  
    /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();	
    $this->configuration['callus_snippet'] = $values['tcxlc_callus'];
  }


}